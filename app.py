from tkinter import *
from tkinter import messagebox
import random
from cryptography.fernet import Fernet
import pungen
# from pungen import UsernameGenerator
import os
import sqlite3
import webbrowser
import unames
import random
import string


from random_password_generator import *
from password import *
from encrypt_decrypt import *
from username_generator import *

def retrieve_input():
    username_keyword = keyword_entry.get('1.0', 'end-1c') 
    # keyword = list(username_keyword.split(" "))
    print(username_keyword)
    username_generate(username_keyword)

# db = sqlite3.connect(
#     host='localhost',
#     user="user",
#     password="password"
# )

# mycursor = password_db.cursor()
# mycursor.execute("CREATE DATABASE pwd_db IF NOT EXISTS ")

# db = sqlite3.connect('pwd_db.db')
# cursor = db.cursor()
# cursor.execute('''
#     CREATE TABLE IF NOT EXISTS passwords (app_id VARCHAR [255] primary key, 
#     platform VARCHAR [50],
#     username VARCHAR [40], 
#     email VARCHAR [255],
#     password VARCHAR [255])
#     ''')
# db.commit()
# db.close()

# master password
m_password = " "

# fernet key
fernet_key = ""

# setting up the tkinter window
root = Tk()
root.title("Password Manager")
root.geometry("600x500+10+20")
# canvas = Canvas(root, height="500", width="600", bg="#123f50")

# Checking the first time run
with open('first_run.txt', 'a+') as f:
    f.seek(0)
    f_contents = f.read()
    if f_contents == '1':
        with open('key.key', 'rb') as kf:
            kf.seek(0)
            kf_contents = kf.read()
            key = kf_contents
            fernet_key =  Fernet(key)

        # get the master password
        with open('pw.txt', 'rb') as pf:
            pf.seek(0)
            pf_contents = pf.read()
            pf_contents = fernet_key.decrypt(pf_contents)
            pf_contents = pf_contents.decode('utf-8')
            m_password = pf_contents
    else:
        root.withdraw()
        f.seek(0)
        f.write('1')
        db = sqlite3.connect('pwd.db')
        cursor = db.cursor()
        cursor.execute('''
            CREATE TABLE IF NOT EXISTS passwords (app_id VARCHAR [255] primary key, 
            platform VARCHAR [50],
            username VARCHAR [40], 
            email VARCHAR [255],
            password VARCHAR [255])
        ''')
        db.commit()
        db.close()


# first time run popup
first_time = Toplevel()
first_time.title("Setup")
first_time.geometry("250x140")

# label for first time run 
labelframe = LabelFrame(first_time, text="Enter the email address:")
labelframe.pack(fill="both", expand="yes")
button_frame = Frame(first_time)
button_frame.pack()
# entry field for email address 
master_email = Entry(labelframe, width=50)
master_email.pack(padx=10, pady=20)
# label for password
labelframe = LabelFrame(first_time, text="enter master password :")
labelframe.pack(fill="both", expand="yes")
button_frame = Frame(first_time)
button_frame.pack()
# entry field for password
master_pass = Entry(labelframe, width=25)
master_pass.pack(padx=10, pady=20)
#submit button for first time run
AddButton = Button(button_frame, text='Set the user', command = save_master_pass)
AddButton.pack()

# function of launching password details
def pwd_table():
    pwdtable = Toplevel()
    pwdtable.title('Passwords')
    pwdtable.geometry('450x700')

    # reloading function for the password table 
    def pwdtable_reload():
        pwd_table.destroy()
        pwd_table()

    # creating labels for the password table
    db_frame = LabelFrame(pwdtable, text="Passwords").pack(fill="both", expand="yes")
    # creating the labels of the rows
    platform_label = Label(db_frame, text="Platform").grid(row=0, column=1)
    username_label = Label(db_frame, text="Username").grid(row=0, column=2)
    email_label = Label(db_frame, text="Email Address").grid(row=0, column=3)
    password_label = Label(db_frame, text="Password").grid(row=0, column=4)

    # accessing db
    decrypt_db()
    db = sqlite3.connect('pwd_db.db')
    cursor = db.cursor()
    cursor.execute("SELECT * FROM passwords WHERE master_userid = :master_userid")

    #display the table from db
    i = 1
    for password_sql in cursor:
        for j in range(len(password_sql)):
            number_line = str(i)
            number_line = number_line + ". "
            l = Label(db_frame, text=number_line)
            l.grid(row=i, column=0)
            e = Entry(db_frame, width=20, fg='blue')
            u = J + 1
            e.grid(row=i, column=u)
            e.insert(END, password_sql[j])
        i += 1
    encrypt_db()


# invisible frames of program
main_frame = Frame(root, padx=5, pady=5)
secondary_frame = Frame(root, padx=5, pady=5)

main_frame.pack(padx=20, pady=20)

# create username generator frame
frame0 = LabelFrame(main_frame, text="USERNAME GENERATOR", width=200,height=100, padx=2, pady=2)
frame0.grid(row=0, column=0)

# scalable slide controller to get the size of username
# char_count = Scale(frame0, from_=0, to=20, orient=HORIZONTAL).grid(row=0, column=0, padx=2, pady=2)
# keyword entry
keyword_entry = Text(frame0, width=15, height=2, )
keyword_entry.grid(rowspan=2, column=0, padx=2, pady=2)

def username_generate(keyword):
    u = list()
    entry= keyword
    for i in range(1):
        u.append(unames.gen_username(entry))
        generated_username = {el for el in u}
        print(generated_username)

    username = Entry(frame0)
    username.insert(END, generated_username)
    username.grid(row=0, column=2)

# username generate button
username_gen = add_button = Button(frame0, text="Generate Username",command=retrieve_input)
username_gen.grid(row=2, column=0)
# username entry 
username_label = Label(frame0, text = "Username: ").grid(row=0, column=1, padx=2, pady=2)

# create password generator frame
frame1 = LabelFrame(main_frame, text="PASSWORD GENERATOR", height="100", padx=2, pady=2)
frame1.grid(row=1, column=0)

# getting alphabetic, digit, andspcial character counts
# labels
def custom_random_pass_gen():
    # characters to generate passwords
    # password_length = int(length_entry.get())
    alphabet = list(string.ascii_letters)
    digits = list(string.digits)
    special_chars = list("!@#$%^&*(),.-=+")

    # character count from each 
    alphabet_count = int(letter_count.get())
    digits_count = int(digit_count.get())
    special_chars_count = int(character_count.get())

    chars_count = alphabet_count + digits_count + special_chars_count

    print("Character Count : {}".format(chars_count))

    # initializing password variable
    password = []

    # picking random alphabet values
    for i in range(alphabet_count):
        password.append(random.choice(alphabet))

    # picking random digit values
    for i in range(digits_count):
        password.append(random.choice(digits))

    # picking special character values
    for i in range(special_chars_count):
        password.append(random.choice(special_chars))
    
    # shuffle the password characters
    random.shuffle(password)
    #for i in range(password):
    print("".join(password))
    display_button.config(text=password)


alphabet_label = Label(frame1, text="Enter the alphabet count : ").grid(row=0, column=0)
digit_label = Label(frame1, text="Enter the digit count : ",).grid(row=1, column=0)
special_char_label = Label(frame1, text="Enter the special character count : ").grid(row=2, column=0)


#entries
letter_count = Entry(frame1, width=5).grid(row=0, column=1, padx=2, pady=2)
digit_count = Entry(frame1, width=5, textvariable=int).grid(row=1, column=1, padx=20, pady=10)
character_count = Entry(frame1, width=5, textvariable=int).grid(row=2, column=1, padx=20, pady=10)

# submit button
# password = add_button = Button(frame1, text="Generate Password", command=custom_random_pass_gen()).grid(row=3, column=1)

# display password
display_password = Label(frame1, text="Passwrod : ", width=50).grid(row=5, column=0, padx=20, pady=10)
display_button = Button(frame1, text="Password").grid(row=5, column=1, padx=20, pady=10)

# frame2 for the password saving
frame2 = LabelFrame(main_frame, text="Save Passwords", height="100", padx=2, pady=2)
frame2.grid(row=2, column=0, padx=2, pady=2)
# labels of the save passwords 
username_label = Label(frame2, text="Username: ").grid(row=0, column=0, padx=2, pady=2)
email_label = Label(frame2, text="Email: ").grid(row=1, column=0, padx=2, pady=2)
password_label = Label(frame2, text="Password: ").grid(row=2, column=0, padx=2, pady=2)
platform_label = Label(frame2, text="Platform: ").grid(row=3, column=0, padx=2, pady=2)
# entries of the save passwords
username_entry = StringVar()

username_entry = Entry(frame2).grid(row=0, column=1, padx=2, pady=2)
email_entry = Entry(frame2).grid(row=1, column=1, padx=2, pady=2)
password_entry = Entry(frame2).grid(row=2, column=1, padx=2, pady=2)
platform_entry = Entry(frame2).grid(row=3, column=1, padx=2, pady=2)

def get_user_pass():
    username = username_entry.get()
    email = email_entry.get()
    password = password_entry.get()
    platform = platform_entry.get()
    save_pass(username, email, password, platform)

# submit button
password_save_button = Button(frame2, text="Save", command=get_user_pass).grid(row=4, column=1, padx=2, pady=2)

# password list
password_list = Button(main_frame, text="Saved Passwords", command=pwd_table).grid(row=2, column=1, padx=2, pady=2)
# password sharing
share_pass = Button(main_frame, text="Share Passwords").grid(row=3, column=1)




#starting the window
root.mainloop()
