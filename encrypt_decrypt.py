# Creating a function for encrypting and decrypting the .db file.
def encrypt_db():
    with open('pwd_db.db', 'rb') as to_encrypt:
        data = to_encrypt.read()
        data = my_fernet.encrypt(data)
        with open('pwd_db.db', "wb") as encrypt:
            encrypt.write(data)


def decrypt_db():
    with open('pwd_db.db', 'rb') as to_decrypt:
        data = to_decrypt.read()
        data = my_fernet.decrypt(data)
        with open('pwd_db.db', 'wb') as decrypt:
            decrypt.write(data)

