import sqlite3

def save_pass():
    decrypt_db()
    db = sqlite3.connect('pwd_db.db')
    cursor = db.cursor()
    cursor.execute("INSERT INTO passwords VALUES ( :platfrom, :username, :email, :password)",
    {
        'Username': username_entry.get(),
        'Email Address': email_entry.get(),
        'Password': password_entry.get(),
        'Platform': platform_entry.get(),
    })
    db.commit()
    db.close()
    encrypt_db()
    # a_pass.destroy()
    alert('Information', 'Password saved successfully.')

def delete_pass():
    def delete_pwd():
        decrypt_db()
        db = sqlite3.connect('pwd.db')
        cursor = db.cursor()
    cursor.execute("DELETE FROM passwords WHERE app_id = :app", {
        'app': app_name_delete.get()
    })
    db.commit()
    db.close()
    encrypt_db()
    pwd_table_reload()
    alert('Information', 'Password Deleted Successfully')


def save_master_pass():
    # master key generate
    with open('key.key', 'wb') as f:
        key = Fernet.generate_key()
        global fernet_key
        fernet_key = Fernet(key)
        f.seek(0)
        f.write(key)

    # Writing the encoded password on the file
    with open('pw.txt', 'wb') as f:
        f.seek(0)
        global m_password
        m_password = m_password.get()
        password_write = m_password.encode('utf-8')
        password_write = fernet_key.encrypt(password_write)
        f.write(password_write)
    First_time.destroy()
    root.deiconify()
    encrypt_db()


